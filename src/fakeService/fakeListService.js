const fakeDatabase = {
  items: [
    {
      id: 'DellInsprion',
      description: 'Dell - Inspiron 15.6 Touch-Screen Laptop - Intel Core i3 - 6GB Memory - 1TB Hard Drive - Black',
      name: 'Dell - Inspiron 15.6 ',
      imageUrl: 'images/notebook1.jpg',
      price: 430
    },
    {
      id: 'HPAMD',
      description: 'HP - 15.6 Laptop - AMD A6-Series - 4GB Memory - 500GB Hard Drive - Black',
      name: 'HP - 15.6',
      imageUrl: 'images/notebook2.jpg',
      price: 310
    },
    {
      id: 'MicrosoftLumia',
      description: 'Microsoft-Lumia-550',
      name: 'Microsoft Lumia 550',
      imageUrl: 'images/notebook3.jpg',
      price: 140
    },
    {
      id: 'AsusIntel',
      description: 'Asus - 15.6 Laptop - Intel Core i3 - 4GB Memory - 1TB Hard Drive - Silver',
      name: 'Asus - 15.6 Laptop',
      imageUrl: 'images/notebook4.jpg',
      price: 425
    },
    {
      id: 'HPIntel',
      description: 'HP - 15.6 Touch-Screen Laptop - Intel Core i5 - 6GB - 1TB Hard Drive - HP textured diamond pattern in black',
      name: 'HP - 15.6',
      imageUrl: 'images/notebook5.jpg',
      price: 925
    },
    {
      id: 'Lenovo',
      description: 'Lenovo - Ideapad 100s 14 Laptop - Intel Celeron - 2GB Memory - 64GB eMMC Flash Storage - Silver',
      name: 'Lenovo - Ideapad 100s 14',
      imageUrl: 'images/notebook6.jpg',
      price: 800
    },
    {
      id: 'HPPavilion',
      description: 'HP - Pavilion 17.3  Laptop - Intel Core i3 - 4GB - 1TB Hard Drive - Horizontal brushing in natural silver',
      name: 'HP - Pavilion 17.3',
      imageUrl: 'images/notebook7.jpg',
      price: 475
    },
    {
      id: 'HPENVY',
      description: 'HP - ENVY 17.3 Touch-Screen Laptop - Intel Core i7 - 16GB Memory - 1TB Hard Drive - Silver',
      name: 'HP - ENVY 17.3',
      imageUrl: 'images/notebook8.jpg',
      price: 160
    }
  ]
};

const fetch = (ms) =>
  new Promise((resolve) => setTimeout(() => resolve(fakeDatabase), ms));

const fetchWithError = (ms) =>
  new Promise((resolve, reject) => setTimeout(() => reject('Error'), ms));

export { fetch, fetchWithError };
