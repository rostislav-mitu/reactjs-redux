window.DellInsprion = {
  description: 'Dell - Inspiron 15.6 Touch-Screen Laptop - Intel Core i3 - 6GB Memory - 1TB Hard Drive - Black',
  name: 'Dell - Inspiron 15.6 ',
  imageUrl: 'images/notebook1.jpg',
  screen: '"15.6" HD (1366 x 768) 16:9 CineCrystal, LCD',
  camera: 'HD Webcam',
  hardDisk: '500 GB HDD',
  price: 430
};

window.HPAMD = {
  description: 'HP - 15.6 Laptop - AMD A6-Series - 4GB Memory - 500GB Hard Drive - Black',
  name: 'HP - 15.6',
  imageUrl: 'images/notebook2.jpg',
  screen: '"15.6" HD (1366 x 768) 16:9 CineCrystal, LCD',
  camera: 'HD Webcam',
  hardDisk: '500 GB HDD',
  price: 310
};

window.MicrosoftLumia = {
  description: 'Microsoft-Lumia-550',
  name: 'Microsoft Lumia 550',
  imageUrl: 'images/notebook3.jpg',
  screen: '"15.6" HD LED (1366x768)',
  camera: '720p / 0.3MP',
  hardDisk: '500 GB',
  price: 140
};

window.AsusIntel = {
  description: 'Asus - 15.6 Laptop - Intel Core i3 - 4GB Memory - 1TB Hard Drive - Silver',
  name: 'Asus - 15.6 Laptop',
  imageUrl: 'images/notebook4.jpg',
  screen: '"15.6" HD (1366 x 768) 16:9 CineCrystal, LCD',
  camera: 'HD Webcam',
  hardDisk: '500 GB HDD',
  price: 425
};

window.HPIntel = {
  description: 'HP - 15.6 Touch-Screen Laptop - Intel Core i5 - 6GB - 1TB Hard Drive - HP textured diamond pattern in black',
  name: 'HP - 15.6',
  imageUrl: 'images/notebook5.jpg',
  screen: '"15.6" HD (1366 x 768) 16:9 CineCrystal, LCD',
  camera: 'HD Webcam',
  hardDisk: '500 GB HDD',
  price: 925
};

window.Lenovo = {
  description: 'Lenovo - Ideapad 100s 14 Laptop - Intel Celeron - 2GB Memory - 64GB eMMC Flash Storage - Silver',
  name: 'Lenovo - Ideapad 100s 14',
  imageUrl: 'images/notebook6.jpg',
  screen: '"15.6" HD (1366 x 768) 16:9 CineCrystal, LCD',
  camera: 'HD Webcam',
  hardDisk: '500 GB HDD',
  price: 800
};

window.HPPavilion = {
  description: 'HP - Pavilion 17.3  Laptop - Intel Core i3 - 4GB - 1TB Hard Drive - Horizontal brushing in natural silver',
  name: 'HP - Pavilion 17.3',
  imageUrl: 'images/notebook7.jpg',
  screen: '"15.6" HD (1366 x 768) 16:9 CineCrystal, LCD',
  camera: 'HD Webcam',
  hardDisk: '500 GB HDD',
  price: 475
};

window.HPENVY = {
  description: 'HP - ENVY 17.3 Touch-Screen Laptop - Intel Core i7 - 16GB Memory - 1TB Hard Drive - Silver',
  name: 'HP - ENVY 17.3',
  imageUrl: 'images/notebook8.jpg',
  screen: '"15.6" HD (1366 x 768) 16:9 CineCrystal, LCD',
  camera: 'HD Webcam',
  hardDisk: '500 GB HDD',
  price: 160
};

const fetch = (ms, id) =>
  new Promise((resolve) => setTimeout(() => resolve(window[id]), ms));

const fetchWithError = (ms) =>
  new Promise((resolve, reject) => setTimeout(() => reject('Error'), ms));

export { fetch, fetchWithError };
