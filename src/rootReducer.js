import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { itemsReducer, visibilityFilter } from './components/ProductListPage/ProductListPageReducer';
import { detailReducer } from './components/ProductDetailPage/ProductDetailPageReducer';
import { cartReducer } from './common/commonReducers';
import { messageReducer } from './components/GlobalMessage/GlobalMessageReducers';

const rootReducer = combineReducers({
  itemsReducer,
  visibilityFilter,
  detailReducer,
  cartReducer,
  messageReducer,
  routing: routerReducer
});

export default rootReducer;
