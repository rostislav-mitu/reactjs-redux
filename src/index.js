import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import 'bootstrap/dist/css/bootstrap.css';
import configureStore from './configureStore';
import ProductListPage from './components/ProductListPage/ProductListPage';
import ProductDetailPage from './components/ProductDetailPage/ProductDetailPageContainer';
import BasketPage from './components/BasketPage/BasketPage';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
  <Provider store={store}>
    <div>
      <Router history={history}>
        <Route path="/" component={ProductListPage} />
        <Route path="/basket" component={BasketPage} />
        <Route path="/:itemID" component={ProductDetailPage} />
      </Router>
    </div>
  </Provider>,
  document.getElementById('root')
);
