import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './rootReducer';
import { loadState, saveState } from './localStorage';
import { addPLPDefaultMessage } from './components/GlobalMessage/index';

const loggerMiddleware = createLogger();

export default function configureStore() {
  const store = createStore(
    rootReducer,
    loadState(),
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  );
  addPLPDefaultMessage(store);
  store.subscribe(() => {
    saveState(store.getState());
    console.log(store.getState());
    console.log('DADADADA');
  });
  return store;
}
