import update from 'immutability-helper';

const isMessageInStore = (listOfMessages, message) => {
  let isMessageInList = false;
  listOfMessages.forEach((messagFromList) => {
    if (messagFromList.id === message.id) {
      isMessageInList = true;
    }
  });
  return isMessageInList;
};

const deleteMessageFromStore = (listOfMessages, messageToDelete) => (
  listOfMessages.filter((messageFromList) => messageFromList.id !== messageToDelete.id)
);

function messageReducer(state = [], action) {
  switch (action.type) {
    case 'ADD_MESSAGE':
      if (!isMessageInStore(state, action.message)) {
        return update(state, { $push: [Object.assign({
          type: 'default',
          isClosable: true,
          isAutoClosable: true,
          restrictions: []
        }, action.message)] });
      }
      return state;
    case 'DELETE_MESSAGE':
      return deleteMessageFromStore(state.slice(), action.message);
    case 'DELETE_ALL_MESSAGES':
      return [];
    default:
      return state;
  }
}

export { messageReducer };
