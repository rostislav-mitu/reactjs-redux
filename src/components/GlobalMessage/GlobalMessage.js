import React from 'react';
import PropTypes from 'prop-types';
import GlobalMessageAlert from './GlobalMessageAlert';
import './style.css';

class GlobalMessage extends React.Component {
  constructor() {
    super();
    this.getValidMessages = this.getValidMessages.bind(this);
  }

  getValidMessages() {
    const {
      messages,
      restriction
    } = this.props;
    return messages.filter(message => {
      if (message.restrictions.length === 0) {
        return true;
      }
      return (
        message.restrictions.indexOf(restriction) > -1
      );
    });
  }

  getAlertClass() {
    const { local } = this.props;
    if (local) {
      return 'localAlertWrapper';
    }
    return 'globalAlertWrapper';
  }

  render() {
    const { children } = this.props;
    const validMessages = this.getValidMessages();
    const globalMesageAlerts = validMessages.map(message => (
      <GlobalMessageAlert key={message.id} message={message} deleteGlobalMessage={this.props.deleteGlobalMessage}>
        {children}
      </GlobalMessageAlert>
    ));
    return (
      <div className={this.getAlertClass()}>
        {globalMesageAlerts}
      </div>
    );
  }
}

GlobalMessage.propTypes = {
  messages: PropTypes.array,
  children: PropTypes.string,
  restriction: PropTypes.string,
  deleteGlobalMessage: PropTypes.func,
  local: PropTypes.string
};

export default GlobalMessage;
