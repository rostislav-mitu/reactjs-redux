import expect from 'expect'
import React from 'react'
import sinon from 'sinon'
import * as actions from './GlobalMessageActions'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

describe("GlobalMessageActions", () => {
    let message = {
        id:"someID",
        type: "warning",
        msg: "Your basket page is empty",
        isClosable: false,
        isAutoClosable: false,
    };
    it("Should return the right action for addMessage", sinon.test(function() {
        let expectedResult = {
            type: "ADD_MESSAGE",
            message
        };
        let action = actions.addMessage(message);
        expect(action).toEqual(expectedResult);
    }));
    it("Should return the right action for deleteMessage", sinon.test(function() {
        let expectedResult = {
            type: "DELETE_MESSAGE",
            message
        };
        let action = actions.deleteMessage(message);
        expect(action).toEqual(expectedResult);
    }));
    it("Should return the right action for deleteAllMessages", sinon.test(function() {
        let expectedResult = {
            type: "DELETE_ALL_MESSAGES"
        };
        let action = actions.deleteAllMessages(message);
        expect(action).toEqual(expectedResult);
    }));
});