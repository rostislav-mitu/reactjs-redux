import expect from 'expect'
import React from 'react'
import { mount } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import GlobalMessageAlert from './GlobalMessageAlert'
import { TIME_TO_DISPLAY, ANIMATION_TIME } from './GlobalMessageConstants'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(message) {
    return mount(
        <GlobalMessageAlert message={message}/>
    );
}

describe("<GlobalMesssageAlert/>", () => {

    const firstMessage = {
        id: "someID",
        type: "warning",
        msg: "Your basket page is empty",
        isClosable: false,
        isAutoClosable: true,
        restrictions: ["productListPage", "productDetailPage"]
    };
    const secondMessage = {
        id: "anotherID",
        type: "succes",
        msg: "succes",
        isClosable: true,
        isAutoClosable: false,
        restrictions: ["basketPage"]
    };
    let displayCloseButtonIfIsNeeded;
    let setAutoClosableTime;
    let closeMessageAlert;
    let displayMessageAlert;
    beforeEach(() => {
        displayCloseButtonIfIsNeeded = sinon.spy(GlobalMessageAlert.prototype, "displayCloseButtonIfIsNeeded");
        setAutoClosableTime = sinon.spy(GlobalMessageAlert.prototype, "setAutoClosableTime");
        closeMessageAlert = sinon.spy(GlobalMessageAlert.prototype, "closeMessageAlert");
        displayMessageAlert = sinon.spy(GlobalMessageAlert.prototype, "displayMessageAlert");
    });
    afterEach(() => {
        displayCloseButtonIfIsNeeded.restore();
        setAutoClosableTime.restore();
        closeMessageAlert.restore();
        displayMessageAlert.restore();
    });



    it("Should render alert", sinon.test(function () {
        const wrapper = setup(firstMessage);
        expect(wrapper.find("div").hasClass("alertMessage")).toBe(true);
    }));
    it("Should call displayCloseButtonIfIsNeeded", sinon.test(function () {
        const wrapper = setup(firstMessage);
        expect(GlobalMessageAlert.prototype.displayCloseButtonIfIsNeeded.calledWith(firstMessage)).toBe(true);
    }));
    it("displayCloseButtonIfIsNeeded should return undefined when isClosable = false", sinon.test(function () {
        const wrapper = setup(firstMessage);
        expect(GlobalMessageAlert.prototype.displayCloseButtonIfIsNeeded.returned(undefined)).toBe(true);
        expect(wrapper.find(".closeBtn").length).toBe(0);
    }));
    it("displayCloseButtonIfIsNeeded should return undefined when isClosable = true", sinon.test(function () {
        setup(secondMessage);
        expect(GlobalMessageAlert.prototype.displayCloseButtonIfIsNeeded.returned(undefined)).toBe(false);
    }));
    it("Should call closeMessageAlert when isAutoClosable = true", sinon.test(function () {
        let clock = sinon.useFakeTimers();
        setup(firstMessage);
        clock.tick(TIME_TO_DISPLAY);
        expect(GlobalMessageAlert.prototype.closeMessageAlert.called).toBe(true);
        clock.restore();
    }));
    it("Should call closeMessageAlert when closeButton is clicked", sinon.test(function () {
        let wrapper = setup(secondMessage);
        wrapper.find(".closeBtn").simulate("click");
        expect(GlobalMessageAlert.prototype.closeMessageAlert.called).toBe(true);
    }));
});