import React from 'react';
import PropTypes from 'prop-types';
import { ANIMATION_TIME, TIME_TO_DISPLAY } from './GlobalMessageConstants';

export default class GlobalMessageAlert extends React.Component {
  constructor() {
    super();
    this.state = {};
    this.setAutoClosableTime = this.setAutoClosableTime.bind(this);
    this.displayMessageAlert = this.displayMessageAlert.bind(this);
    this.closeMessageAlert = this.closeMessageAlert.bind(this);
  }

  componentDidMount() {
    const { message } = this.props;
    setTimeout(() => {
      this.displayMessageAlert(message);
      this.setAutoClosableTime(message);
    }, 0);
  }

  setAutoClosableTime() {
    const { message } = this.props;
    if (message.isAutoClosable) {
      setTimeout(() => {
        this.closeMessageAlert(message);
      }, TIME_TO_DISPLAY);
    }
  }

  closeMessageAlert() {
    const {
      message,
      deleteGlobalMessage
    } = this.props;
    const { alertClassName } = this.state;
    this.setState({
      alertClassName: `${alertClassName} fadeEffect`
    });
    setTimeout(() => {
      this.setState({
        alertClassName: `${alertClassName} closedAlert`
      });
      deleteGlobalMessage(message);
    }, ANIMATION_TIME);
  }

  displayMessageAlert() {
    const { message } = this.props;
    this.setState({
      alertClassName: message.type
    });
  }

  displayCloseButtonIfIsNeeded() {
    const { message } = this.props;
    if (message.isClosable) {
      return <span className="closeBtn" tabIndex="0" role="button" onClick={this.closeMessageAlert}>&times;</span>;
    }
    return undefined;
  }

  render() {
    const {
      message,
      children
    } = this.props;
    const { alertClassName } = this.state;
    return (
      <div className={`alertMessage ${alertClassName}`}>
        {
          this.displayCloseButtonIfIsNeeded(message)
        }
        {message.msg}
        {children}
      </div>
    );
  }
}

GlobalMessageAlert.contextTypes = {
  store: PropTypes.object
};

GlobalMessageAlert.propTypes = {
  message: PropTypes.object,
  children: PropTypes.string,
  deleteGlobalMessage: PropTypes.func
};
