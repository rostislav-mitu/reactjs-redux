import { addMessage, deleteMessage, deleteAllMessages } from './GlobalMessageActions';

const addPLPDefaultMessage = (store) => {
  const message = {
    id: 'PLP',
    msg: 'Please buy 2 products with 10% sale',
    isAutoClosable: false,
    restrictions: ['productListPage'],
    global: false
  };
  store.dispatch(addMessage(message));
};

const addGlobalMessage = (store, message) => {
  store.dispatch(addMessage(message));
};

const deleteGlobalMessage = (store, message) => {
  store.dispatch(deleteMessage(message));
};

const deleteAllGlobalMessages = (store) => {
  store.dispatch(deleteAllMessages());
};

export { addPLPDefaultMessage, addGlobalMessage, deleteGlobalMessage, deleteAllGlobalMessages };
