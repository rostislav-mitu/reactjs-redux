const addMessage = message => (
  {
    type: 'ADD_MESSAGE',
    message
  }
);

const deleteMessage = message => (
  {
    type: 'DELETE_MESSAGE',
    message
  }
);

const deleteAllMessages = () => (
  {
    type: 'DELETE_ALL_MESSAGES'
  }
);

export { addMessage, deleteMessage, deleteAllMessages };
