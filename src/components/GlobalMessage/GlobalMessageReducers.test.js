import expect from 'expect'
import sinon from 'sinon'
import * as reducers from './GlobalMessageReducers'
import * as actions from './GlobalMessageActions'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

describe("GlobalMessageReducers", () => {
    const firstMessage = {
        id:"someID",
        type: "warning",
        msg: "Your basket page is empty",
        isClosable: false,
        isAutoClosable: false,
    };
    const secondMessage = {
        id:"anotherID",
        type: "succes",
        msg: "succes",
        isAutoClosable: false,
    };

    it("should add the message in the store if it isn't", sinon.test(function() {
        const action = actions.addMessage(firstMessage);
        let initialState = [];
        const newState = reducers.messageReducer(initialState, action);
        expect(newState.length).toBe(1);
        expect(newState[0].id).toBe(firstMessage.id);
    }));
    it("shouldn't add the message in the store if it is already in there" ,sinon.test(function() {
        const action = actions.addMessage(firstMessage);
        let initialState = [];
        initialState.push(firstMessage);
        const newState = reducers.messageReducer(initialState, action);
        expect(newState.length).toBe(1);
    }));
    it("should delete the message from the store if it is in there" ,sinon.test(function() {
        const action = actions.deleteMessage(firstMessage);
        let initialState = [];
        initialState.push(firstMessage);
        initialState.push(secondMessage);
        const newState = reducers.messageReducer(initialState, action);
        expect(newState.length).toBe(1);
        expect(newState[0].id).toNotBe(firstMessage.id);
    }));
    it("should delete all the messages from the store properly" ,sinon.test(function() {
        const action = actions.deleteAllMessages();
        let initialState = [];
        initialState.push(firstMessage);
        initialState.push(secondMessage);
        const newState = reducers.messageReducer(initialState, action);
        expect(newState.length).toBe(0);
    }));
    it("should return the previous state when action type is wrong" ,sinon.test(function() {
        let initialState = [];
        initialState.push(firstMessage);
        initialState.push(secondMessage);
        const newState = reducers.messageReducer(initialState, {type: ""});
        expect(newState).toBe(initialState);
    }));
});