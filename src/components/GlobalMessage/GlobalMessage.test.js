import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import GlobalMessage from './GlobalMessage'
import GlobalMessageAlert from './GlobalMessageAlert'
import { TIME_TO_DISPLAY } from './GlobalMessageConstants'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(messages, restriction) {
    return shallow(
        <GlobalMessage messages={messages} restriction={restriction}/>
    );
}

describe("<GlobalMesssage/>", () => {

    const firstMessage = {
        id:"someID",
        type: "warning",
        msg: "Your basket page is empty",
        isClosable: false,
        isAutoClosable: false,
        restrictions: ["productListPage", "productDetailPage"]
    };
    const secondMessage = {
        id:"anotherID",
        type: "succes",
        msg: "succes",
        isAutoClosable: false,
        restrictions: ["basketPage"]
    };

    let restriction = '';

    it("Should render 0 alerts when messages length = 0", sinon.test(function() {
        const wrapper = setup([]);
        expect(wrapper.find(GlobalMessageAlert).length).toBe(0);
    }));
    it("Should render 1 alert when messages length = 1 and respect the restrictions", sinon.test(function() {
        restriction = 'productListPage';
        const wrapper = setup([firstMessage], restriction);
        expect(wrapper.find(GlobalMessageAlert).length).toBe(1);
    }));
    it("Should render 1 alert when messages length = 2 and 1 of them doesn't respect the restrictions", sinon.test(function() {
        restriction = 'productListPage';
        const wrapper = setup([firstMessage, secondMessage], restriction);
        expect(wrapper.find(GlobalMessageAlert).length).toBe(1);
    }));
    it("Should render 2 alerts when messages length = 2 and both respect the restrictions", sinon.test(function() {
        restriction = 'productListPage';
        secondMessage.restrictions.push("productListPage");
        const wrapper = setup([firstMessage, secondMessage], restriction);
        expect(wrapper.find(GlobalMessageAlert).length).toBe(2);
    }));
    it("Should render 0 alerts when messages length = 2 and both doesn't respect the restrictions", sinon.test(function() {
        restriction = 'otherRestriction';
        const wrapper = setup([firstMessage, secondMessage], restriction);
        expect(wrapper.find(GlobalMessageAlert).length).toBe(0);
    }));
});