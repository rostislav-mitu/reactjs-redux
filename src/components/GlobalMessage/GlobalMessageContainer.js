import { connect } from 'react-redux';
import GlobalMessage from './GlobalMessage';
import { deleteMessage } from './GlobalMessageActions';

const mapStateToProps = (state) => (
  {
    messages: state.messageReducer,
    test: state.test
  }
);

const mapDispatchToProps = (dispatch) => ({
  deleteGlobalMessage: (message) => dispatch(deleteMessage(message))
});

export default connect(mapStateToProps, mapDispatchToProps)(GlobalMessage);
