import React from 'react';
import PropTypes from 'prop-types';
import { deleteAllGlobalMessages } from '../GlobalMessage/index';

class LogoutButton extends React.Component {
  constructor() {
    super();
    this.deleteAllMessagesFromStore = this.deleteAllMessagesFromStore.bind(this);
  }
  deleteAllMessagesFromStore() {
    const { store } = this.context;
    deleteAllGlobalMessages(store);
  }
  render() {
    return (
      <div className="logout">
        <button className="logoutButton" onClick={this.deleteAllMessagesFromStore}>Logout</button>
      </div>
    );
  }
}

LogoutButton.contextTypes = {
  store: PropTypes.object
};

export default LogoutButton;
