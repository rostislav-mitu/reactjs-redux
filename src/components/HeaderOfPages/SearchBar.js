import React from 'react';

const SearchBar = () => (
  <div className="arrangeFit">
    <div className="searchBar">
      <div className="searchDropdown">
        <button>
          All
        </button>
      </div>
      <div className="searchWrapper">
        <input className="searchInput" placeholder="Search..." type="text" />
      </div>
      <div className="searchButton">
        <i className="fa fa-search" />
      </div>
    </div>
  </div>
);

export default SearchBar;
