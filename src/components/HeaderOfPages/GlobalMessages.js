import React from 'react';
import GlobalMessage from '../GlobalMessage/GlobalMessage';

const GlobalMessages = () => (
  <div className="alertContainer">
    <GlobalMessage>
      !!!
    </GlobalMessage>
  </div>
);

export default GlobalMessages;
