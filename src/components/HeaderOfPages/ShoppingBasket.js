import React from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import ShoppingCart from './ShoppingCart';

const ShoppingBasket = (props) => {
  const { items } = props;
  return (
    <div className="shoppingCart">
      <Link to={'basket'}>
        <i className="fa fa-shopping-cart" aria-hidden="true" />
      </Link>
      <span className="basketQty">{items.length}</span>
      <ShoppingCart items={items} />
    </div>
  );
};

ShoppingBasket.propTypes = {
  items: PropTypes.array
};

export default ShoppingBasket;
