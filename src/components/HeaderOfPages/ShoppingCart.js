/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import DeleteProductButton from '../../common/DeleteProductButton';
import { addGlobalMessage, deleteGlobalMessage } from '../GlobalMessage/index';

class ShoppingCart extends React.Component {
  componentDidMount() {
    this.forceUpdate();
  }
  componentDidUpdate() {
    this.updateMessageStore();
  }
  updateMessageStore() {
    const { items } = this.props;
    const { store } = this.context;
    const message = {
      id: 'basketMessage',
      type: 'warning',
      msg: 'Your basket page is empty',
      isClosable: false,
      isAutoClosable: false,
      restrictions: ['basketPage']
    };
    if (items.length === 0) {
      addGlobalMessage(store, message);
    } else {
      deleteGlobalMessage(store, message);
    }
  }
  render() {
    const { items } = this.props;
    return (
      <div className="cart">
        {
          items.map(item => (
            <div className="cartProduct" key={item.name}>
              <img src={require("./" + item.imageUrl)} alt="" height="60px" width="60px" />
              <span>{item.name}</span><br />
              <span>{item.count}</span>
              <DeleteProductButton item={item} />
            </div>
          ))
        }
      </div>
    );
  }
}

ShoppingCart.contextTypes = {
  store: PropTypes.object
};
ShoppingCart.propTypes = {
  items: PropTypes.array
};

export default ShoppingCart;
