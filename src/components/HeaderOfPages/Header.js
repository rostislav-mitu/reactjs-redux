import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Logo from './Logo';
import SearchBar from './SearchBar';
import AccountInfo from './AccountInfo';
import ShoppingBasket from './ShoppingBasket';
import LogoutButton from './LogoutButton';
import './s.css';

const mapStateToProps = (state) => (
  {
    items: state.cartReducer.items
  }
);

const Header = (props) => {
  const { items } = props;
  return (
    <header>
      <Link to="/">
        <Logo />
      </Link>
      <div className="headerContent">
        <SearchBar />
        <AccountInfo />
        <LogoutButton />
        <ShoppingBasket items={items} />
      </div>
    </header>
  );
};

Header.propTypes = {
  items: PropTypes.array
};

export default connect(mapStateToProps)(Header);
