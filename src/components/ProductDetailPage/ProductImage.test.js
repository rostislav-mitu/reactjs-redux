/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { mount, shallow } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import ProductImage from './ProductImage'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(item) {
    return shallow(<ProductImage item={item}/>);
}

describe("ProductImage", () => {
    let item = {
        id: "someLink",
        imageUrl: "images/notebook2.jpg"
    };
    it("src of Image should be the same as item's imageUrl", sinon.test(function() {
        const wrapper = setup(item);
        expect('images/' + wrapper.find("img").prop("src")).toBe(item.imageUrl);
    }))
});