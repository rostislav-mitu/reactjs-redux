function detailReducer(state = {
  isFetching: false,
  item: {}
}, action) {
  switch (action.type) {
    case 'REQUEST_ITEM':
      return Object.assign({}, state, {
        isFetching: true,
        item: {}
      });
    case 'RECEIVE_ITEM':
      return Object.assign({}, state, {
        isFetching: false,
        item: action.post
      });
    case 'RECEIVE_ERROR':
      return Object.assign({}, state, {
        isFetching: false,
        errorMessage: action.errorMessage
      });
    default:
      return state;
  }
}

export {
  detailReducer
};
