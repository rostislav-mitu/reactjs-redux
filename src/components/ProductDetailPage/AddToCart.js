import React from 'react';
import PropTypes from 'prop-types';
import QuantitySelector from './QuantitySelector';
import AddToCartButton from './AddToCartButton';

class AddToCart extends React.Component {
  constructor() {
    super();
    this.state = {
      itemQuantity: 1
    };
    this.updateQuantitySelector = this.updateQuantitySelector.bind(this);
  }
  updateQuantitySelector(e) {
    const target = e.currentTarget;
    this.setState({
      itemQuantity: parseInt(target.value, 10) || 1
    });
  }
  render() {
    const { item } = this.props;
    const { itemQuantity } = this.state;
    return (
      <div className="addToCart">
        <QuantitySelector update={this.updateQuantitySelector} />
        <AddToCartButton item={item} itemQuantity={itemQuantity} />
      </div>
    );
  }
}

AddToCart.propTypes = {
  item: PropTypes.object
};

export default AddToCart;
