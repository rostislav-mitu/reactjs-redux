import { fetch, fetchWithError } from '../../fakeService/fakeDetailService';

const requestItem = () => (
  {
    type: 'REQUEST_ITEM'
  }
);

const receiveItem = (response) => (
  {
    type: 'RECEIVE_ITEM',
    post: response
  }
);

const receiveError = (receiveMessage) => (
  {
    type: 'RECEIVE_ITEM',
    receiveMessage
  }
);

const fetchItem = (id, withError) => (
  dispatch => {
    let globalFetch = fetch;
    dispatch(requestItem());
    if (withError) {
      globalFetch = fetchWithError;
    }
    return globalFetch(0, id)
      .then(response => dispatch(receiveItem(response)))
      .catch(errorMessage => dispatch(receiveError(errorMessage)));
  }
);

export {
  fetchItem,
  requestItem,
  receiveItem,
  receiveError
};
