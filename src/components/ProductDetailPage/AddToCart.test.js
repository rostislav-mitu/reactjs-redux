/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { mount, shallow } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import AddToCart from './AddToCart'
import QuantitySelector from './QuantitySelector'
import AddToCartButton from './AddToCartButton'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(item) {
    return shallow(<AddToCart item={item}/>);
}

function renderInput(update) {
    return shallow(<input onChange={update}/>)
}


describe("ProductDetailPage -> AddToCart", () => {
    let item = {
        id: "someLink",
        price: 420
    };
    function simulateQuantitySelectorChange(quantity) {
        const wrapper = setup(item);
        let instanceOfWrapper = wrapper.instance();
        const input = renderInput(instanceOfWrapper.updateQuantitySelector);
        const event = {
            currentTarget: {
                value: quantity
            }
        };
        input.simulate("change", event);
        return instanceOfWrapper.state.itemQuantity;
    }
    it("should render QuantitySelector and AddToCartButton", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find(QuantitySelector).length).toBe(1);
        expect(wrapper.find(AddToCartButton).length).toBe(1);
    }));
    it("should update itemQuantity when QuantitySelector input value is changed", sinon.test(function() {
        let quantity = "7";
        expect(simulateQuantitySelectorChange(quantity)).toBe(parseInt(quantity || 1));
    }));
    it("should update itemQuantity with default value when QuantitySelector input is empty", sinon.test(function() {
        let quantity = "";
        expect(simulateQuantitySelectorChange(quantity)).toBe(parseInt(quantity || 1));
    }));

});