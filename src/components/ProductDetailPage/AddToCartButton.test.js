/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { mount, shallow } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import AddToCartButton from './AddToCartButton'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(item) {
    return shallow(<AddToCartButton item={item}/>);
}
describe("ProductDetailPage -> AddToCartButton", () => {
    let item = {
        id: "someLink",
        price: 420
    };
    it("should render a Button to add item in basket", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find("button").length).toBe(1);
    }));
    it("should call 'AddToCart' when button is clicked", sinon.test(function() {
        let stub = this.stub(AddToCartButton.prototype, "addToCart");
        const wrapper = setup(item);
        wrapper.find("button").simulate("click");
        expect(stub.calledOnce).toBe(true);
    }));
});