/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';

const ProductImage = (props) => {
  const { item } = props;
  return (
    <div className="productImage">
      <img src={require("./" + item.imageUrl)} alt="" height="350px" width="450px" />
    </div>
  );
};

ProductImage.propTypes = {
  item: PropTypes.object
};

export default ProductImage;
