/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import QuantitySelector from './QuantitySelector'
import sinonTest from 'sinon-test'
import { Label, Input } from 'reactstrap'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);



function setup(update) {
    return shallow(<QuantitySelector update={update}/>);
}

describe("ProductDetailPage -> <QuantitySelector>", () => {
    it("should render input to select quantity", sinon.test(function() {
        const wrapper = setup(this.spy());
        expect(wrapper.find(Input).exists()).toBe(true);
    }));
    it("should render label for input", sinon.test(function() {
        const wrapper = setup(this.spy());
        expect(wrapper.find(Label).exists()).toBe(true);
    }));
    it("should call 'onChange' callback when value of input is changed", sinon.test(function() {
        let updateSpy = this.spy();
        const wrapper = setup(updateSpy);
        const event = {
            currentTarget: {
                value: "7"
            }
        };
        wrapper.find(Input).simulate("change", event);
        expect(updateSpy.calledOnce).toBe(true);
        expect(updateSpy.calledWith(event)).toBe(true);
    }));
});