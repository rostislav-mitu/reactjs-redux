/* eslint-disable */
import expect from 'expect'
import sinon from 'sinon'
import * as reducers from './ProductDetailPageReducer'
import * as actions from './ProductDetailPageActions'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

describe("ProductDetailPageReducers", () => {
    const initialState = {
        isFetching: false,
        item: {
            id: 1,
            price: 420
        }
    };
    it("isFetching property of detailReducer state should be true when data is fetching", sinon.test(function() {
        initialState.isFetching = false;
        const action = actions.requestItem();
        const newState = reducers.detailReducer(initialState, action);
        expect(newState.isFetching).toBe(true);
    }));
    it("detailReducer should return the right value when data is received" ,sinon.test(function() {
        initialState.isFetching = true;
        const response = {
            id: 4,
            price: 220
        };
        const action = actions.receiveItem(response);
        const newState = reducers.detailReducer(initialState, action);

        expect(newState.isFetching).toBe(false);
        expect(newState.item).toBe(response);
    }));
    // it("detailReducer should return the right value when error is occured in data fetching", sinon.test(function() {
    //     initialState.isFetching = true;
    //     let error = "Error";
    //     const action = actions.receiveError(error);
    //     const newState = reducers.detailReducer(initialState, action);
    //     expect(newState.isFetching).toBe(false);
    //     expect(newState.errorMessage).toBe(error);
    // }));
    it("detailReducer should return the default store when action type doesn't corespond and store is undefined", sinon.test(function() {
        const defaultStore = {
            isFetching: false,
            item: {}
        };
        const action = {
            type: "DEFAULT"
        };
        const newState = reducers.detailReducer(undefined, action);
        expect(Object.keys(newState).length).toBe(Object.keys(defaultStore).length);
        expect(newState.isFetching).toBe(defaultStore.isFetching);
        expect(Object.keys(newState.item).length).toBe(Object.keys(defaultStore.item).length);
    }));
});