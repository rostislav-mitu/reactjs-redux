/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import ProductDescription from './ProductDescription'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(item) {
    return shallow(<ProductDescription item={item}/>);
}

describe("ProductDetailPage -> ProductDescription", () => {
    let item = {
        id: "someLink",
        price: 420
    };
    it("description of Product should be the same as item's properties", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.instance().props.item).toBe(item);
    }));
});