/* eslint-disable */
import expect from 'expect'
import React from 'react'
import sinon from 'sinon'
import * as actions from './ProductDetailPageActions'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

describe("ProductDetailPageActions-> functions that return actions", () => {
    // it("Should return the right action for requestItem", sinon.test(function() {
    //     let expectedResult = {
    //         type: "REQUEST_ITEM"
    //     };
    //     let action = actions.requestItem();
    //     expect(action).toEqual(expectedResult);
    // }));
    // it("Should return the right action for receiveItem", sinon.test(function() {
    //     let fakeService = {
    //         id: 1,
    //         price: 420
    //     };
    //     let expectedResult = {
    //         type: "RECEIVE_ITEM",
    //         post: fakeService
    //     };
    //     let action = actions.receiveItem(fakeService);
    //     expect(action.post).toBe(expectedResult.post);
    //     expect(action.type).toBe(expectedResult.type);
    // }));
    // it("Should return the right action for receiveError", sinon.test(function() {
    //     let errorMessage = "Error";
    //     let expectedResult = {
    //         type: "RECEIVE_ERROR",
    //         errorMessage
    //     };
    //     let action = actions.receiveError(errorMessage);
    //     expect(action).toEqual(expectedResult);
    // }));
});

describe("ProductDetailPageActions-> functions that dispatch actions", () => {
    const middleware = [thunk];
    const mockStore = configureMockStore(middleware);
    const verifyActions = (store, expectedActions, done) => {
        const actionsPerformed = store.getActions();
        expect(actionsPerformed[0].type).toBe(expectedActions[0].type);
        expect(actionsPerformed[1].type).toBe(expectedActions[1].type);
        done();
    };

    // it("should dispatch REQUEST_ITEM and RECEIVE_ITEM when fetchItem is called", function(done) {
    //     const expectedActions = [
    //         {
    //             type: "REQUEST_ITEM"
    //         },
    //         {
    //             type: "RECEIVE_ITEM",
    //             posts: {}
    //         }
    //     ];
    //     const store = mockStore({});
    //     store.dispatch(actions.fetchItem()).then(() => {
    //         verifyActions(store, expectedActions, done);
    //     });
    // });
    // it("should dispatch REQUEST_ITEM and RECEIVE_ERROR when fetchItem is called", function(done) {
    //     const expectedActions = [
    //         {
    //             type: "REQUEST_ITEM"
    //         },
    //         {
    //             type: "RECEIVE_ERROR",
    //             posts: {}
    //         }
    //     ];
    //     const store = mockStore({});
    //     store.dispatch(actions.fetchItem(undefined, true)).then(() => {
    //         verifyActions(store, expectedActions, done);
    //     });
    // });
});