import React from 'react';
import PropTypes from 'prop-types';
import { Label, Input } from 'reactstrap';

const QuantitySelector = (props) => {
  const { update } = props;
  return (
    <div>
      <Label for="quantity">Quantity:</Label>
      <Input type="text" onChange={update} id="quantity" placeholder="Enter a number of items" />
    </div>
  );
};

QuantitySelector.propTypes = {
  update: PropTypes.func
};

export default QuantitySelector;

