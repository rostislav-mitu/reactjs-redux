/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { mount, shallow } from 'enzyme'
import sinon from 'sinon'
import ProductDescription from './ProductDescription'
import AddToCart from './AddToCart'
import Spinner from '../../common/Spinner'
import ProductImage from './ProductImage'
import Main from './Main'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);


function setup(item) {
    return shallow(<Main item={item}/>);
}

describe("ProductDetailPage -> <Main>", () => {
    let item = {
        id: "SomeID",
        price: "420",
        imageUrl: "image.jpg"
    };
    it("should render 'ProductImage', 'ProductDescription', 'Add to cart' components", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find(ProductImage).length).toBe(1);
        expect(wrapper.find(ProductDescription).length).toBe(1);
        expect(wrapper.find(AddToCart).length).toBe(1);
        expect(wrapper.find(Spinner).length).toBe(0);
    }));
});