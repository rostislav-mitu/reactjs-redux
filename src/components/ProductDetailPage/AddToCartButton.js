import React from 'react';
import PropTypes from 'prop-types';
import { addItemToCart } from '../../common/commonActions';
import { addGlobalMessage } from '../GlobalMessage/index';

class AddToCartButton extends React.Component {
  constructor() {
    super();
    this.addToCart = this.addToCart.bind(this);
  }
  addToCart() {
    const { store } = this.context;
    const { itemQuantity } = this.props;
    const { item } = this.props;
    store.dispatch(addItemToCart(item, itemQuantity));
    this.addSuccesGlobalMessage(store);
  }
  addSuccesGlobalMessage(store) {
    const message = {
      id: 'AddToCart',
      type: 'succes',
      msg: 'Item was added succesufuly',
      restrictions: ['productListPage', 'productDetailPage']
    };
    addGlobalMessage(store, message);
  }
  render() {
    return (
      <div>
        <button onClick={this.addToCart}>Add to card</button>
      </div>
    );
  }
}

AddToCartButton.contextTypes = {
  store: PropTypes.object
};

AddToCartButton.propTypes = {
  itemQuantity: PropTypes.number,
  item: PropTypes.item
};

export default AddToCartButton;
