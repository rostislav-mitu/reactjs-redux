import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'reactstrap';

const itemDescriptions = ['screen', 'camera', 'hardDisck', 'description'];

const ProductDescription = (props) => {
  const { item } = props;
  return (
    <div>
      <div>
        <h3>{item.name}</h3>
      </div>
      <div className="itemPrice">
        <h2>${props.item.price}</h2>
      </div>
      <Table>
        <tbody>
          {itemDescriptions.map((key) => (
            <tr key={key}>
              <td>{key}</td>
              <td>{item[key]}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

ProductDescription.propTypes = {
  item: PropTypes.object
};

export default ProductDescription;
