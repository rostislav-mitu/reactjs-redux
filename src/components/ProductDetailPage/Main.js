import React from 'react';
import PropTypes from 'prop-types';
import ProductImage from './ProductImage';
import ProductDescription from './ProductDescription';
import AddToCart from './AddToCart';

const Main = (props) => {
  const { item } = props;
  return (
    <div>
      <div className="productDetail">
        <div>
          <ProductImage item={item} />
          <ProductDescription item={item} />
          <AddToCart item={item} />
        </div>
      </div>
    </div>
  );
};

Main.propTypes = {
  item: PropTypes.object
};

export default Main;
