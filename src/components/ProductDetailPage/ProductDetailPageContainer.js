import { connect } from 'react-redux';
import ProductDetailPage from './ProductDetailPage';

const mapStateToProps = (state) => (
  {
    item: state.detailReducer.item
  }
);

export default connect(mapStateToProps)(ProductDetailPage);
