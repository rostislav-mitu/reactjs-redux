import React from 'react';
import PropTypes from 'prop-types';
import './style.css';
import { fetchItem } from './ProductDetailPageActions';
import Main from './Main';
import Header from '../HeaderOfPages/Header';
import GlobalMessage from '../GlobalMessage/GlobalMessageContainer';

class ProductDetailPage extends React.Component {
  componentWillMount() {
    const { store } = this.context;
    const { params } = this.props;
    const { itemID } = params;
    store.dispatch(fetchItem(itemID));
  }
  render() {
    const { item } = this.props;
    return (
      <div id="productDetailPage">
        <Header />
        <GlobalMessage restriction="productDetailPage">
          !!!
        </GlobalMessage>
        <div className="detailWrapper">
          <Main item={item} />
        </div>
      </div>
    );
  }
}

ProductDetailPage.contextTypes = {
  store: PropTypes.object
};

ProductDetailPage.propTypes = {
  params: PropTypes.object,
  item: PropTypes.object
};

export default ProductDetailPage;
