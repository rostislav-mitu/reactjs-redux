/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { mount, shallow } from 'enzyme'
import sinon from 'sinon'
import ProductDetailPage from './ProductDetailPage'
import Main from './Main'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);



function setup() {
    let item = {
        id: "SomeID",
        price: "420"
    };
    return shallow(<ProductDetailPage item={item}/>);
}

describe("<ProductDetailPage/>", () => {
    let stub;
    beforeEach(() => {
        stub = sinon.stub(ProductDetailPage.prototype,"componentWillMount");
    });
    afterEach(() => {
        stub.restore();
    });
    it("Should render<Main/> component", sinon.test(function() {
        const wrapper = setup();
        expect(wrapper.find(Main).length).toBe(1);
    }));
});