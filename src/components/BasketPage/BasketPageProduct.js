/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import DeleteProductButton from '../../common/DeleteProductButton';

const BasketPageProduct = (props) => {
  const { item } = props;
  return (
    <div className="cartProduct">
      <img src={require("./" + item.imageUrl)} alt="" height="60px" width="60px" />
      <span>{item.name}</span><br />
      <span>{item.count}</span>
      <DeleteProductButton item={item} />
    </div>
  );
};

BasketPageProduct.propTypes = {
  item: PropTypes.object
};

export default BasketPageProduct;
