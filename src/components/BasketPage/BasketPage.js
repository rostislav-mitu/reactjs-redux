import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from '../HeaderOfPages/Header';
import BasketPageProduct from './BasketPageProduct';
import GlobalMessage from '../GlobalMessage/GlobalMessageContainer';
import './style.css';

const mapStateToProps = (state) => (
  {
    items: state.cartReducer.items
  }
);

const BasketPage = (props) => {
  const { items } = props;
  return (
    <div>
      <Header />
      <GlobalMessage restriction="basketPage">
        !!!
      </GlobalMessage>
      <div className="basketPage">
        {
          items.map(item => (
            <BasketPageProduct item={item} key={item.name} />
          ))
        }
      </div>
    </div>
  );
};

BasketPage.propTypes = {
  items: PropTypes.array
};

export default connect(mapStateToProps)(BasketPage);
