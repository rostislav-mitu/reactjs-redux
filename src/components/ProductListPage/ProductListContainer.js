import { connect } from 'react-redux';
import ProductList from './ProductList';
import getVisibleItems from './ProductListPageUtils';

const getItems = getVisibleItems();

const mapStateToProps = (state) => (
  {
    items: getItems(
      state.itemsReducer,
      state.visibilityFilter
    )
  }
);

const ProductListContainer = connect(
  mapStateToProps,
  null
)(ProductList);

export default ProductListContainer;
