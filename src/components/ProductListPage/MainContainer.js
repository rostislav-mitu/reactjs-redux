import { connect } from 'react-redux';
import Main from './Main';

const mapStateToProps = (state) => (
  {
    isFetching: state.itemsReducer.isFetching
  }
);

export default connect(mapStateToProps)(Main);
