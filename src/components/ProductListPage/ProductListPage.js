import React from 'react';
import Main from './MainContainer';
import SideBarFilter from './SideBarFilter';
import Header from '../HeaderOfPages/Header';
import GlobalMessage from '../GlobalMessage/GlobalMessageContainer';

const ProductListPage = () => (
  <div className="productListPage" id="productListPage">
    <Header />
    <GlobalMessage restriction="productListPage" local="">
      !!!
    </GlobalMessage>
    <Main />
    <SideBarFilter />
  </div>
);

export default ProductListPage;
