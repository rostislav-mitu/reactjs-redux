// import React from 'react'
// import * as slickUtils from './SlickUtils'
//
// export default class SlickDots extends React.Component {
//     constructor() {
//         super();
//         this.clickHandler = this.clickHandler.bind(this);
//         this.activeSlide = {};
//     }
//
//     clickHandler(e) {
//         let self = this;
//         let currentActiveDot = e.currentTarget;
//         let dotIndex = parseInt(currentActiveDot.dataset.pointindex);
//         let activeSlideIndex = parseInt(self.activeSlide.dataset.index);
//         let slidesToMove = activeSlideIndex - dotIndex;
//         let moveSlides = slickUtils.changeAndGetPrevSlide;
//         if(dotIndex > activeSlideIndex) {
//             slidesToMove = dotIndex - activeSlideIndex;
//             moveSlides = slickUtils.changeAndGetNextSlide;
//         }
//         slickUtils.moveSliders(moveSlides, slidesToMove, self.activeSlide, self.props.updateActiveSlider)
//             .then((activeSlide) => self.activeSlide = activeSlide);
//     }
//
//
//     getSliders() {
//         let slides = this.props.slickContainer.children;
//         if(slides === undefined) {
//             return [];
//         } else {
//             return [].slice.call(slides);
//         }
//     }
//
//     getActiveSlideIndex() {
//         if(this.props.slickContainer.children !== undefined) {
//             this.activeSlide = document.getElementsByClassName("active")[0];
//             return document.getElementsByClassName("active")[0].dataset.index;
//         }
//     }
//
//     render() {
//         let slides = this.getSliders();
//         let activeSlideIndex = this.getActiveSlideIndex();
//         return (
//             <ul className="slickDots">
//                 {slides.map((slider, index) => {
//                     setTimeout(() => {
//                         slickUtils.changeActivePoint(document.querySelectorAll("[data-pointindex='" + activeSlideIndex + "']")[0].children[0]);
//                     }, 0);
//                     return (
//                         <li onClick={this.clickHandler} data-pointindex={index + 1} key={index}>
//                             <button className={"dots"}>
//
//                             </button>
//                         </li>
//                     )
//                 })}
//             </ul>
//         )
//     }
//
// }
