/* eslint-disable */
import React from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';

const ProductImage = (props) => {
  const { item } = props;
  return (
    <Link to={item.id}>
      <img src={require('./' + item.imageUrl)} alt="" height="150px" width="200px" />
    </Link>
  );
};

ProductImage.propTypes = {
  item: PropTypes.object
};

export default ProductImage;
