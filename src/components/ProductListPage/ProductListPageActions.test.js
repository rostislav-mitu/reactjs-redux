/* eslint-disable */
import expect from 'expect'
import sinon from 'sinon'
import * as actions from './ProductListPageActions'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

describe("ProductListPageActions-> functions that return actions", () => {
    it("Should return the right action for requestItems", sinon.test(function() {
        let expectedResult = {
            type: "REQUEST_ITEMS"
        };
        let action = actions.requestItems();
        expect(action).toEqual(expectedResult);
    }));
    it("Should return the right action for receiveItems", sinon.test(function() {
        let fakeService = {
            items: [
                {
                    id: 1,
                    price: 420
                },
                {
                    id: 2,
                    price: 120
                },
                {
                    id: 3,
                    price: 350
                }
            ]
        };
        let expectedResult = {
            type: "RECEIVE_ITEMS",
            posts: fakeService.items
        };
        let action = actions.receiveItems(fakeService);
        expect(action).toEqual(expectedResult);
    }));
    it("Should return the right action for receiveError", sinon.test(function() {
        let errorMessage = "Error";
        let expectedResult = {
            type: "RECEIVE_ERROR",
            errorMessage
        };
        let action = actions.receiveError(errorMessage);
        expect(action).toEqual(expectedResult);
    }));
    it("Should return the right action when filterItems is called", sinon.test(function() {
        let filter = {
            key: "SHOW_BY_BRAND",
            value: ["HP"]
        };
        let expectedResult = {
            type: "SET_VISIBILITY_FILTER",
            filter
        };
        let action = actions.filterItems(filter);
        expect(action).toEqual(expectedResult);
    }));
});

describe("ProductListPageActions-> functions that dispatch actions", () => {
    const middleware = [thunk];
    const mockStore = configureMockStore(middleware);

    const verifyActions = (store, expectedActions, done) => {
        const actionsPerformed = store.getActions();
        expect(actionsPerformed[0].type).toBe(expectedActions[0].type);
        expect(actionsPerformed[1].type).toBe(expectedActions[1].type);
        done();
    };

    it("should dispatch REQUEST_ITEMS and RECEIVE_ITEMS when fetchItems is called", function(done) {
        const expectedActions = [
            {
                type: "REQUEST_ITEMS"
            },
            {
                type: "RECEIVE_ITEMS",
                posts: []
            }
        ];
        const store = mockStore({}, expectedActions);
        store.dispatch(actions.fetchItems()).then(() => {
            verifyActions(store, expectedActions, done);
        });
    });
    it("should dispatch REQUEST_ITEMS and RECEIVE_ERROR when fetchItems is called", function(done) {
        const FETCH_WITH_ERROR = true;
        const expectedActions = [
            {
                type: "REQUEST_ITEMS"
            },
            {
                type: "RECEIVE_ERROR"
            }
        ];
        const store = mockStore({}, expectedActions);
        store.dispatch(actions.fetchItems(FETCH_WITH_ERROR)).then(() => {
            verifyActions(store, expectedActions, done);
        });
    });
});