// import React from 'react'
// import SlickDots from './SlickDots'
// import LeftArrow from './LeftArrow'
// import RightArrow from './RightArrow'
// import * as slickUtils from './SlickUtils'
//
// export default class Slick extends React.Component {
//     constructor() {
//         super();
//         this.next = this.next.bind(this);
//         this.prev = this.prev.bind(this);
//         this.updateActiveSlide = this.updateActiveSlide.bind(this);
//         this.state = {
//             slickContainer: {},
//             activeSlide: {}
//         }
//     }
//
//     componentDidMount() {
//         let slickContainer = document.getElementsByClassName("slickContainer")[0];
//         const FIRST_CHILD_INDEX = 1;
//         let activeSlide = slickUtils.addAndGetActiveSlider(FIRST_CHILD_INDEX);
//         slickUtils.addNextAndPreviousSliders(activeSlide);
//         this.setState({
//             slickContainer,
//             activeSlide
//         });
//     }
//
//     updateActiveSlide(activeSlide) {
//         this.setState({
//             activeSlide: activeSlide
//         })
//     }
//
//     addClassesAndIndexes() {
//         return React.Children.map(this.props.children,
//             (child, index) => React.cloneElement(child, {
//                 "data-index": index + 1,
//                 className: "slickSlide"
//             })
//         );
//     }
//
//     next() {
//         slickUtils.moveSliders(slickUtils.changeAndGetNextSlide, this.props.scroll, this.state.activeSlide, this.updateActiveSlide, this.props.children.length);
//     }
//
//     prev() {
//         slickUtils.moveSliders(slickUtils.changeAndGetPrevSlide, this.props.scroll, this.state.activeSlide, this.updateActiveSlide, this.props.children.length);
//     }
//
//     render() {
//         const childrenWithProps = this.addClassesAndIndexes();
//         return (
//             <div className="slick">
//                 <LeftArrow onClick={this.prev.bind(this)}/>
//                 <RightArrow onClick={this.next.bind(this)}/>
//                 <div className="slickContainer">
//                     {childrenWithProps}
//                 </div>
//                 <SlickDots activeSlider={this.state.activeSlide} updateActiveSlider={this.updateActiveSlide} slickContainer={this.state.slickContainer}/>
//             </div>
//         )
//     }
// }
