import React from 'react';
import PropTypes from 'prop-types';
import { ButtonDropdown, DropdownMenu, DropdownToggle } from 'reactstrap';
import './css/sideBar.css';
import BrandFilter from './BrandFilter';
import PriceFilter from './PriceFilter';
import DesktopBreakpoint from '../../common/responsive-utilities/DesktopBreakpoint';
import TabletBreakpoint from '../../common/responsive-utilities/TabletBreakpoint';

class SideBarFilter extends React.Component {
  constructor() {
    super();
    this.toggleFilter = this.toggleFilter.bind(this);
    this.state = {
      collapsed: true
    };
  }
  toggleFilter() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }
  render() {
    return (
      <aside>
        <DesktopBreakpoint>
          <BrandFilter />
          <PriceFilter />
        </DesktopBreakpoint>
        <TabletBreakpoint>
          <ButtonDropdown isOpen={!this.state.collapsed} toggle={this.toggleFilter}>
            <DropdownToggle caret>
              Filter
            </DropdownToggle>
            <DropdownMenu>
              <BrandFilter />
              <PriceFilter />
            </DropdownMenu>
          </ButtonDropdown>
        </TabletBreakpoint>
      </aside>
    );
  }
}

SideBarFilter.contextTypes = {
  store: PropTypes.object
};

export default SideBarFilter;
