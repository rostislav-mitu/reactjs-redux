function itemsReducer(state = {
  isFetching: false,
  items: []
}, action) {
  switch (action.type) {
    case 'REQUEST_ITEMS':
      return Object.assign({}, state, {
        isFetching: true,
      });
    case 'RECEIVE_ITEMS':
      return Object.assign({}, state, {
        isFetching: false,
        items: action.posts
      });
    case 'RECEIVE_ERROR':
      return Object.assign({}, state, {
        isFetching: false,
        errorMessage: action.errorMessage
      });
    default:
      return state;
  }
}


function visibilityFilter(state = {
  key: 'SHOW_ALL',
  value: ''
}, action) {
  switch (action.type) {
    case 'SET_VISIBILITY_FILTER':
      return action.filter;
    default:
      return state;
  }
}

export {
  itemsReducer,
  visibilityFilter
};
