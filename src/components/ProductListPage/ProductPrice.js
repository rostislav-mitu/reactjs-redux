import React from 'react';
import PropTypes from 'prop-types';

const ProductPrice = (props) => {
  const { item } = props;
  return (
    <div className="price">
      <span>${item.price}</span>
    </div>
  );
};

ProductPrice.propTypes = {
  item: PropTypes.object
};

export default ProductPrice;
