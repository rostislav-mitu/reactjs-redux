/* eslint-disable */
import expect from 'expect'
import sinon from 'sinon'
import * as reducers from './ProductListPageReducer'
import * as actions from './ProductListPageActions'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

describe("ProductListPageReducers", () => {
    const initialState = {
        isFetching: false,
        items: [
            {
                id: 1,
                price: 420
            },
            {
                id: 2,
                price: 120
            }
        ]
    };
    it("itemsReducer should return the default state when state is undefined and action doesn't corespond", sinon.test(function() {
        const defaultState = {
            isFetching: false,
            items: []
        };
        const action = {
            type: "DEFAULT"
        };
        const newState = reducers.itemsReducer(undefined, action);
        expect(newState.key).toBe(defaultState.key);
        expect(newState.value).toBe(defaultState.value);
    }));
    it("isFetching property of itemsReducer state should be true when data is fetching", sinon.test(function() {
        initialState.isFetching = false;
        const action = actions.requestItems();
        const newState = reducers.itemsReducer(initialState, action);
        expect(newState.isFetching).toBe(true);
    }));
    it("itemsReducer should return the right value when data is received" ,sinon.test(function() {
        initialState.isFetching = true;
        const response = Object.assign({}, initialState);
        response.items.push({
            id: 3,
            price:200
        });
        const action = actions.receiveItems(response);
        const newState = reducers.itemsReducer(initialState, action);

        expect(newState.isFetching).toBe(false);
        expect(newState.items).toBe(response.items);
    }));
    it("itemsReducer should return the right value when error is occured in data fetching", sinon.test(function() {
        initialState.isFetching = true;
        let error = "Error";
        const action = actions.receiveError(error);
        const newState = reducers.itemsReducer(initialState, action);
        expect(newState.isFetching).toBe(false);
        expect(newState.errorMessage).toBe(error);
    }));
    it("should return the initialState when action doesn't corespond in visibilityFilter", sinon.test(function() {
        const defaultState = {
            key: "SHOW_ALL",
            value: ""
        };
        const action = {
            type: "DEFAULT"
        };
        const newState = reducers.visibilityFilter(undefined, action);
        expect(newState.key).toBe(defaultState.key);
        expect(newState.value).toBe(defaultState.value);

    }));
    it("visibilityFilter reducer should return the right state when action is disptached", sinon.test(function() {
        const filter = {
            key: "SHOW_BY_BRAND",
            value: ["HP"]
        };
        const action = actions.filterItems(filter);
        const newState = reducers.visibilityFilter(undefined, action);
        expect(newState).toBe(filter);

    }));

});