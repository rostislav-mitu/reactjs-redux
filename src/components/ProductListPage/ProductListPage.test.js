/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import ProductListPage from './ProductListPage'
import Main from './MainContainer'
import SideBarFilter from './SideBarFilter'

function setup() {
    return shallow(<ProductListPage/>);
}

describe("ProductListPage via React Test Utils", () => {
   it("should render one Main component ", () => {
       const wrapper = setup();
       expect(wrapper.find(Main).length).toBe(1);
   });

   it("should render one SideBarFilter component ", () => {
        const wrapper = setup();
        expect(wrapper.find(SideBarFilter).length).toBe(1);
    });
});
