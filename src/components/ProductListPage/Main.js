import React from 'react';
import PropTypes from 'prop-types';
import ProductListContainer from './ProductListContainer';
import './css/main.css';
import { fetchItems } from './ProductListPageActions';
import Spinner from '../../common/Spinner';

class Main extends React.Component {
  componentWillMount() {
    const { store } = this.context;
    store.dispatch(fetchItems());
  }
  render() {
    const { isFetching } = this.props;
    return (
      <main>
        {isFetching ?
        (() =>
          <Spinner />)()
          :
        (() =>
          <ProductListContainer />)()}
      </main>
    );
  }
}

Main.contextTypes = {
  store: PropTypes.object
};

Main.propTypes = {
  isFetching: PropTypes.bool
};

export default Main;
