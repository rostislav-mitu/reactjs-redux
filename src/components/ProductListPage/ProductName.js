import React from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';

const ProductName = (props) => {
  const { item } = props;
  return (
    <p className="productName">
      <Link to={item.id}>
        {item.name}
      </Link>
    </p>
  );
};

ProductName.propTypes = {
  item: PropTypes.object
};

export default ProductName;
