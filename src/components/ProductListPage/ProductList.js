import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import Product from './Product';
import NoResultByFilter from './NoResultByFilter';

const ProductList = (props) => {
  const { items } = props;
  return (
    <div>
      <Row>
        {items.map(item => {
          if (item === null) { return <NoResultByFilter key="unique" />; }
          return (
            <Col key={item.id}>
              <Product key={item.id} item={item} />
            </Col>
          );
        })}
      </Row>
    </div>
  );
};

ProductList.propTypes = {
  items: PropTypes.array
};

export default ProductList;
