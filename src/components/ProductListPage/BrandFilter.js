import React from 'react';
import PropTypes from 'prop-types';
import { filterItems } from './ProductListPageActions';

class BrandFilter extends React.Component {
  constructor() {
    super();
    this.brandCheckBoxes = [];
    this.setFilterByBrand = this.setFilterByBrand.bind(this);
  }

  setFilterByBrand() {
    const checkedBoxes = this.brandCheckBoxes.filter(item => item.checked);
    const checkedBoxesValues = checkedBoxes.map(item => item.value);
    const { store } = this.context;
    store.dispatch(filterItems({
      key: 'SHOW_BY_BRAND',
      value: checkedBoxesValues
    }));
  }
  render() {
    const brands = ['Asus', 'Microsoft', 'HP', 'Dell', 'Lenovo'];
    return (
      <div className="filter">
        <p className="filterName">
          Brands
        </p>
        <div>
          {brands.map((brand, index) => (
            <div key={brand} className="option">
              <input
                type="checkbox"
                id={brand}
                value={brand}
                ref={(input) => { this.brandCheckBoxes[index] = input; }}
                onChange={this.setFilterByBrand}
              />
              <label htmlFor={brand}>{brand}</label>
            </div>))
          }
        </div>
      </div>
    );
  }
}

BrandFilter.contextTypes = {
  store: PropTypes.object
};

export default BrandFilter;
