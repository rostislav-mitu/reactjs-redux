import React from 'react';

const NoResultByFilter = () => (
  <div>No result found</div>
);

export default NoResultByFilter;
