/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import AddToCart from './AddToCart'
import sinonTest from 'sinon-test'
import { Button } from 'reactstrap'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(item) {
    return shallow(<AddToCart item={item}/>);
}

describe("<AddToCart/>", () => {
    let item = {
        id: "someId",
        price: "425"
    };
    it("Should call 'addItem' function with specific parameter when button is clicked", sinon.test(function() {
        let addItemToCart = this.stub(AddToCart.prototype, "addItem");
        const wrapper = setup(item);
        wrapper.find(Button).simulate("click");
        expect(addItemToCart.calledOnce).toBe(true);
        expect(addItemToCart.calledWith(item)).toBe(true);
    }));
});