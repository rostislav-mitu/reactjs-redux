/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import NoResultByFilter from './NoResultByFilter'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);



function setup() {
    return shallow(<NoResultByFilter/>);
}

describe("ProductListPage -> <NoResultByFilter/>", () => {
    it("Should render correctly itself", sinon.test(function() {
        const wrapper = setup();
        expect(wrapper.exists()).toBe(true);
    }));
});