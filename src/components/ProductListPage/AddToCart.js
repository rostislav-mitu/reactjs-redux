import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { addItemToCart } from '../../common/commonActions';
import { NUMBER_OF_ITEMS_TO_ADD } from './ProductListPageConstants';
import { fetch } from '../../fakeService/fakeDetailService';
import { addGlobalMessage } from '../GlobalMessage/index';


class AddToCart extends React.Component {
  constructor() {
    super();
    this.addItem = this.addItem.bind(this);
  }
  addItem(item) {
    const self = this;
    const { store } = this.context;
    fetch(0, item.id).then((response) => {
      store.dispatch(addItemToCart(response, NUMBER_OF_ITEMS_TO_ADD));
      self.addMessageToStore(store);
    });
  }
  addMessageToStore(store) {
    const message = {
      id: 'AddToCart',
      type: 'succes',
      msg: 'Item was added succesufuly',
      isClosable: true,
      isAutoClosable: true,
      restrictions: ['productListPage', 'productDetailPage']
    };
    addGlobalMessage(store, message);
  }
  render() {
    const { item } = this.props;
    return (
      <div className="addToCard">
        <Button color="primary" onClick={() => { this.addItem(item); }}>Add to cart</Button>
      </div>
    );
  }
}

AddToCart.contextTypes = {
  store: PropTypes.object
};

AddToCart.propTypes = {
  item: PropTypes.object
};

export default AddToCart;
