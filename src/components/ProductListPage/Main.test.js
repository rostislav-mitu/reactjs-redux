/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import Main from './Main'
import Spinner from '../../common/Spinner'
import ProductListContainer from './ProductListContainer'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);



function setup(isFetching) {
    let props = {
        isFetching
    };
    return shallow(<Main {...props}/>);
}

describe("ProductListPage -> <Main/>", () => {
    let stub;
    beforeEach(() => {
        stub = sinon.stub(Main.prototype,"componentWillMount");
    });
    afterEach(() => {
        stub.restore();
    });
    it("Should render Spinner when data is fetching", sinon.test(function() {
        const wrapper = setup(true);
        expect(wrapper.find(Spinner).length).toBe(1);
        expect(wrapper.find(ProductListContainer).length).toBe(0);
    }));
    it("Should render ProductContainer when data is fetched", sinon.test(function() {
        const wrapper = setup(false);
        expect(wrapper.find(Spinner).length).toBe(0);
        expect(wrapper.find(ProductListContainer).length).toBe(1);
    }));
    it("Should call 'componentWillMount' when <Main> is rendered", sinon.test(function() {
        setup(false);
        expect(stub.calledOnce).toBe(true);
    }));
});