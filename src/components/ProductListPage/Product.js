import React from 'react';
import PropTypes from 'prop-types';
import AddToCart from './AddToCart';
import ProductImage from './ProductImage';
import ProductName from './ProductName';
import ProductPrice from './ProductPrice';

const Product = (props) => {
  const { item } = props;
  return (
    <div className="product" key={item.id}>
      <ProductImage item={item} />
      <ProductName item={item} />
      <ProductPrice item={item} />
      <AddToCart item={item} />
    </div>
  );
};

Product.propTypes = {
  item: PropTypes.object
};

export default Product;
