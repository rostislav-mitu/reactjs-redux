/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import SideBarFilter from './SideBarFilter'
import PriceFilter from './PriceFilter'
import BrandFilter from './BrandFilter'

function setup() {
    return shallow(<SideBarFilter/>);
}

describe("SideBarFilter", () => {
    it("should render two BrandFilter component ", () => {
        const wrapper = setup();
        expect(wrapper.find(BrandFilter).length).toBe(2);
    });

    it("should render two PriceFilter component ", () => {
        const wrapper = setup();
        expect(wrapper.find(PriceFilter).length).toBe(2);
    });
});
