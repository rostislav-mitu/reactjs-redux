import { fetch, fetchWithError } from '../../fakeService/fakeListService';

function requestItems() {
  return {
    type: 'REQUEST_ITEMS'
  };
}

function receiveItems(response) {
  return {
    type: 'RECEIVE_ITEMS',
    posts: response.items
  };
}

function receiveError(errorMessage) {
  return {
    type: 'RECEIVE_ERROR',
    errorMessage
  };
}

function filterItems(filter) {
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  };
}

function fetchItems(withError) {
  return dispatch => {
    let globalFetch = fetch;
    dispatch(requestItems());
    if (withError) {
      globalFetch = fetchWithError;
    }
    return globalFetch(0)
      .then(response => dispatch(receiveItems(response)))
      .catch(errorMessage => dispatch(receiveError(errorMessage)));
  };
}

export {
  fetchItems,
  filterItems,
  requestItems,
  receiveItems,
  receiveError
};
