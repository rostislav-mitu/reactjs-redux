import React from 'react';
import PropTypes from 'prop-types';
import { filterItems } from './ProductListPageActions';
import { PRICE_RANGE } from './ProductListPageConstants';

class PriceFilter extends React.Component {
  constructor() {
    super();
    this.priceCheckBoxes = [];
    this.setFilterByPrice = this.setFilterByPrice.bind(this);
    this.getPriceFilterFromCheckboxes = this.getPriceFilterFromCheckboxes.bind(this);
    this.getPriceFilterFromInputs = this.getPriceFilterFromInputs.bind(this);
  }

  setFilterByPrice(getValue) {
    const { store } = this.context;
    store.dispatch(filterItems({
      key: 'SHOW_BY_PRICE',
      value: getValue()
    }));
  }

  getPriceFilterFromCheckboxes() {
    const priceCheckedBoxes = this.priceCheckBoxes.filter(item => item.checked);
    return priceCheckedBoxes.map(item => {
      const values = item.value.split(',');
      return {
        from: parseInt(values[0], 10),
        to: parseInt(values[1], 10)
      };
    });
  }

  getPriceFilterFromInputs() {
    return [{
      from: parseInt(this.from.value, 10),
      to: parseInt(this.to.value, 10)
    }];
  }
  render() {
    const priceRange = PRICE_RANGE;
    return (
      <div className="filter">
        <p className="filterName">
            Price
        </p>
        <div className="priceFilter">
          <span className="currency">$</span>
          <input
            type="number"
            ref={(from) => { this.from = from; }}
          />
          to <span className="currency">$</span>
          <input
            type="number"
            ref={(to) => { this.to = to; }}
          />
          <button onClick={() => this.setFilterByPrice(this.getPriceFilterFromInputs)}>Go</button>
          {(() => {
            let prevPrice = 0;
            return (
              priceRange.map((price, index) => (
                <div key={`key + ${price}`} className="option">
                  <input
                    type="checkbox"
                    className="priceCheckbox"
                    id={`range + ${price}`}
                    ref={(input) => { this.priceCheckBoxes[index] = input; }}
                    onChange={() => { this.setFilterByPrice(this.getPriceFilterFromCheckboxes); }}
                    value={[prevPrice, price]}
                  />
                  <label htmlFor={`range ${price}`}>{`$ ${prevPrice} - $  ${(prevPrice = price)}`}</label>
                </div>
              ))
            );
          })()}
        </div>
      </div>
    );
  }
}

PriceFilter.contextTypes = {
  store: PropTypes.object
};

export default PriceFilter;
