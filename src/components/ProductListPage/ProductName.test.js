/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import ProductName from './ProductName'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(item) {
    return shallow(<ProductName item={item}/>);
}

describe("ProductName", () => {
    let item = {
        id: "someLink",
        name: "someName"
    };
    it("text of Link should be the same as item's name", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find("Link").children().text()).toBe(item.name);
    }));
    it("'to' prop of Link should be the same as item's id", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find("Link").prop("to")).toBe(item.id);
    }));
});