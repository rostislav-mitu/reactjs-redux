/* eslint-disable */
import expect from 'expect'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import getVisibleItems from './ProductListPageUtils'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);


describe("ProductListPageUtils", () => {

    const fakeDatabase = {
        items: [
            {
                name: "Dell - Inspiron 15.6 ",
                price:430
            },
            {
                name:"HP - 15.6",
                price:310
            },
            {
                name:"Microsoft Lumia 550",
                price:140
            },
            {
                name:"Asus - 15.6 Laptop",
                price:425
            },
            {
                name:"HP - 15.6",
                price:925
            },
            {
                name: "Lenovo - Ideapad 100s 14",
                price:800
            },
            {
                name:"HP - Pavilion 17.3",
                price:475
            },
            {
                name:"HP - ENVY 17.3",
                price:160
            }
        ]
    };
    let getItems;
    beforeEach(() => {
        getItems = sinon.spy(getVisibleItems());
    });
    it("should return the right value when items are not filtered", sinon.test(function() {
        getItems(fakeDatabase, {
            key: "SHOW_ALL"
        });
        expect(getItems.returned(fakeDatabase.items)).toBe(true);
    }));
    it("should return the right value when items are filtered by one name", sinon.test(function() {
        getItems(fakeDatabase, {
            key: "SHOW_BY_BRAND",
            value: ["HP"]
        });
        let expectedResult = [
            {
                name:"HP - 15.6",
                price:310
            },
            {
                name:"HP - 15.6",
                price:925
            },
            {
                name:"HP - Pavilion 17.3",
                price:475
            },
            {
                name:"HP - ENVY 17.3",
                price:160
            }
        ];
        expect(getItems.returned(expectedResult)).toBe(true);
    }));
    it("should return the right value when items are filtered by more names", sinon.test(function() {
        getItems(fakeDatabase, {
            key: "SHOW_BY_BRAND",
            value: ["HP", "Dell"]
        });
        let expectedResult = [
            {
                name: "Dell - Inspiron 15.6 ",
                price:430
            },
            {
                name:"HP - 15.6",
                price:310
            },
            {
                name:"HP - 15.6",
                price:925
            },
            {
                name:"HP - Pavilion 17.3",
                price:475
            },
            {
                name:"HP - ENVY 17.3",
                price:160
            }
        ];
        expect(getItems.returned(expectedResult)).toBe(true);
    }));

    it("should return the right value when items are filtered by one price", sinon.test(function() {
        getItems(fakeDatabase, {
            key: "SHOW_BY_PRICE",
            value: [
                {
                    from: 0,
                    to: 400
                }
            ]
        });
        let expectedResult = [
            {
                name:"HP - 15.6",
                price:310
            },
            {
                name:"Microsoft Lumia 550",
                price:140
            },
            {
                name:"HP - ENVY 17.3",
                price:160
            }
        ];
        expect(getItems.returned(expectedResult)).toBe(true);
    }));

    it("should return the right value when items are filtered by more prices", sinon.test(function() {
        getItems(fakeDatabase, {
            key: "SHOW_BY_PRICE",
            value: [
                {
                    from: 0,
                    to: 400
                },
                {
                    from: 900,
                    to: 1000
                }
            ]
        });
        let expectedResult = [
            {
                name:"HP - 15.6",
                price:310
            },
            {
                name:"Microsoft Lumia 550",
                price:140
            },
            {
                name:"HP - 15.6",
                price:925
            },
            {
                name:"HP - ENVY 17.3",
                price:160
            }
        ];
        expect(getItems.returned(expectedResult)).toBe(true);
    }));
    it("should return the right value when items are filtered by price and name", sinon.test(function() {
        getItems(fakeDatabase, {
            key: "SHOW_BY_PRICE",
            value: [
                {
                    from: 0,
                    to: 400
                },
                {
                    from: 900,
                    to: 1000
                }
            ]
        });
        getItems(fakeDatabase, {
            key: "SHOW_BY_BRAND",
            value: ["HP", "Dell"]
        });
        let expectedResult = [
            {
                name:"HP - 15.6",
                price:310
            },
            {
                name:"HP - 15.6",
                price:925
            },
            {
                name:"HP - ENVY 17.3",
                price:160
            }
        ];
        expect(getItems.returned(expectedResult)).toBe(true);
    }));
    it("should return the right value when filtered items length is 0", sinon.test(function() {
        getItems(fakeDatabase, {
            key: "SHOW_BY_PRICE",
            value: [
                {
                    from: 500,
                    to: 600
                }
            ]
        });
        getItems(fakeDatabase, {
            key: "SHOW_BY_BRAND",
            value: []
        });
        expect(getItems.returned([null])).toBe(true);
    }));
    it("should return the right value when checkboxes are deselected", sinon.test(function() {
        getItems(fakeDatabase, {
            key: "SHOW_BY_PRICE",
            value: []
        });
        getItems(fakeDatabase, {
            key: "SHOW_BY_BRAND",
            value: []
        });
        expect(getItems.returned(fakeDatabase.items)).toBe(true);
    }));

});