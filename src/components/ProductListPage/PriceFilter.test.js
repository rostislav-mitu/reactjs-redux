/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { mount } from 'enzyme'
import sinon from 'sinon'
import PriceFilter from './PriceFilter'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup() {
    return mount(<PriceFilter/>);
}

describe("<PriceFilter/>", () => {
    let setFilterByPrice;
    beforeEach(() => {
        setFilterByPrice = sinon.stub(PriceFilter.prototype, "setFilterByPrice").callsFake((callback) => {
            callback();
        });
    });
    afterEach(() => {
        setFilterByPrice.restore();
    });
    it("should call 'setFilterByPrice' when checkbox is changed ", sinon.test(function() {
        const wrapper = setup();
        wrapper.find("input").last().simulate("change");
        expect(setFilterByPrice.calledOnce).toBe(true);
    }));
    it("should call 'getPriceFilterFromCheckboxes' when checkbox is changed ", sinon.test(function() {
        let getPriceFilterFromCheckboxes = this.spy(PriceFilter.prototype, "getPriceFilterFromCheckboxes");
        const wrapper = setup();
        wrapper.find("input").last().simulate("change");
        expect(getPriceFilterFromCheckboxes.calledOnce).toBe(true);
    }));
    // it("should call 'setFilterByPrice' when Ok button is pressed ", sinon.test(function() {
    //     const wrapper = setup();
    //     wrapper.find("button").simulate("click");
    //     expect(setFilterByPrice.calledOnce).toBe(true);
    // }));
    // it("should call 'getPriceFilterFromInputs' when Ok button is pressed ", sinon.test(function() {
    //     let getPriceFilterFromInputs = this.spy(PriceFilter.prototype, "getPriceFilterFromInputs");
    //     const wrapper = setup();
    //     wrapper.find("button").simulate("click");
    //     expect(getPriceFilterFromInputs.calledOnce).toBe(true);
    // }));
    // it("'getPriceFilterFromInputs' should return the right value when Ok button is pressed", sinon.test(function() {
    //     let getPriceFilterFromInputs = this.spy(PriceFilter.prototype, "getPriceFilterFromInputs");
    //     let expectedResult = [{
    //         from: 10,
    //         to: 100
    //     }];
    //     const wrapper = setup();
    //     wrapper.instance().refs.from.value = "10";
    //     wrapper.instance().refs.to.value = "100";
    //     wrapper.find("button").simulate("click");
    //     expect(getPriceFilterFromInputs.returned(expectedResult)).toBe(true);
    // }));
    it("'getPriceFilterFromCheckboxes' should return the right value when last checkbox is checked", sinon.test(function() {
        let getPriceFilterFromCheckboxes = this.spy(PriceFilter.prototype, "getPriceFilterFromCheckboxes");
        let expectedResult = [{
            from: 0,
            to: 400
        }, {
            from: 400,
            to: 500
        }];
        const wrapper = setup();
        wrapper.instance().priceCheckBoxes[0].value = "0, 400";
        wrapper.instance().priceCheckBoxes[1].value = "400, 500";
        wrapper.instance().priceCheckBoxes[0].checked = true;
        wrapper.instance().priceCheckBoxes[1].checked = true;
        wrapper.find(".priceCheckbox").first().simulate("change");
        expect(getPriceFilterFromCheckboxes.returned(expectedResult)).toBe(true);
    }));

});