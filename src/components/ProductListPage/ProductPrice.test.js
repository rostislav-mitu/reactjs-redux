/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import ProductPrice from './ProductPrice'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(item) {
    return shallow(<ProductPrice item={item}/>);
}

describe("ProductName", () => {
    let item = {
        id: "someLink",
        price: 420
    };
    it("price of Product should be the same as item's price", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find("span").childAt(1).text()).toBe(item.price.toString());
    }));
});