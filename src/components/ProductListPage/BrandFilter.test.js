/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { mount } from 'enzyme'
import sinon from 'sinon'
import BrandFilter from './BrandFilter'

function setup() {
    return mount(<BrandFilter/>);
}

describe("<BrandFilter/>", () => {
    it("should call 'setFilterByBrand' when checkbox is changed ", () => {
        sinon.stub(BrandFilter.prototype, "setFilterByBrand");
        const wrapper = setup();
        wrapper.find("input").first().simulate("change");
        expect(BrandFilter.prototype.setFilterByBrand.calledOnce).toBe(true);
    });
});