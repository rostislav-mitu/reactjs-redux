// const moveSliders = (moveSlider, slidersNrToMove, activeSlider, updateActiveSlide, slidersCount) => {
//     let promise = moveSlider(slidersCount, activeSlider);
//     promise.then(updateActiveSlide);
//     while (slidersNrToMove > 1) {
//         slidersNrToMove --;
//         promise = promise.then(moveSlider.bind(null, slidersCount));
//         promise.then(updateActiveSlide);
//     }
//     return promise;
// };
//
// const changeAndGetNextSlide = (slidersCount, activeSlide) => {
//     let nextIndex = getNextIndex(activeSlide, slidersCount);
//     let nextActiveSlide = addAndGetActiveSlider(nextIndex);
//     addNextAndPreviousSliders(nextActiveSlide);
//     addAnimationClassesForNextClick(activeSlide);
//     return new Promise((resolve) => {
//         setTimeout(() => {
//             removeActiveSliders(activeSlide, deleteAnimationClassesFromNextClick);
//             resolve(nextActiveSlide);
//         }, 800);
//     });
// };
//
// const changeAndGetPrevSlide = (slidersCount, activeSlide) => {
//     let prevIndex = getPrevIndex(activeSlide, slidersCount);
//     let nextActiveSlide = addAndGetActiveSlider(prevIndex);
//     addNextAndPreviousSliders(nextActiveSlide);
//     addAnimationClassesForPrevClick(activeSlide);
//     return new Promise((resolve) => {
//         setTimeout(() => {
//             removeActiveSliders(activeSlide ,deleteAnimationClassesFromPrevClick);
//             resolve(nextActiveSlide);
//         }, 800);
//     });
// };
//
// const removeActiveSliders = (activeSlide, deleteAnimationClasses) => {
//     activeSlide.classList.remove("active");
//     try{
//         activeSlide.nextSibling.classList.remove("next");
//     } catch(e) {
//         activeSlide.parentElement.children[0].classList.remove("next");
//     }
//     try{
//         activeSlide.previousSibling.classList.remove("previous");
//     } catch(e) {
//
//     }
//     deleteAnimationClasses();
// };
//
// const addAndGetActiveSlider = (slideIndex) => {
//     let activeSlide = document.querySelectorAll("[data-index='" + slideIndex + "']")[0];
//     activeSlide.classList.add("active");
//     return activeSlide;
// };
//
// const addNextAndPreviousSliders = (activeSlide, slidesToShow) => {
//     slidesToShow = 3;
//     let slidesContainer = activeSlide.parentElement;
//     let nextSlide = activeSlide;
//     let previousSlide = activeSlide;
//     let isNextSlide = true;
//     for(let i = 0; i < slidesToShow - 1; i++) {
//         if(isNextSlide) {
//             nextSlide = addNextSlider(nextSlide, slidesContainer);
//             isNextSlide = false;
//         } else {
//             previousSlide = addPreviousSlider(previousSlide, slidesContainer);
//             isNextSlide = true;
//         }
//     }
// };
//
// const addNextSlider = (activeSlide, slidesContainer) => {
//     let nextSlide = activeSlide.nextSibling;
//     let lastSlide;
//     if(nextSlide) {
//         nextSlide.classList.add("next");
//         return nextSlide;
//     } else {
//         lastSlide = getAndMoveFirstSlideAtTheEnd(slidesContainer);
//         lastSlide.classList.add("next");
//         return lastSlide;
//     }
// };
//
// const addPreviousSlider = (activeSlide, slidesContainer) => {
//     let previousSlide = activeSlide.previousSibling;
//     let firstSlide;
//     if(previousSlide) {
//         previousSlide.classList.add("previous");
//         return previousSlide;
//     } else {
//         firstSlide = getAndMoveLastSlideAtTheStart(slidesContainer);
//         firstSlide.classList.add("previous");
//         return firstSlide;
//     }
// };
//
// const deleteAnimationClassesFromNextClick = () => {
//     try {
//         document.getElementsByClassName("prevOfPrev")[0].classList.remove("prevOfPrev");
//         document.getElementsByClassName("nextOfNext")[0].classList.remove("nextOfNext");
//     } catch(e) {
//
//     }
// };
//
// const deleteAnimationClassesFromPrevClick = () => {
//     try {
//         document.getElementsByClassName("prevOfPrev1")[0].classList.remove("prevOfPrev1");
//     } catch(e) {
//
//     }
//     try{
//         document.getElementsByClassName("nextOfNext1")[0].classList.remove("nextOfNext1");
//     } catch(e) {
//
//     }
// };
//
//
// const changeActivePoint = (point) => {
//     let activePoint = document.getElementsByClassName("activePoint")[0];
//     if(activePoint) {
//         activePoint.classList.remove("activePoint");
//     }
//     point.classList.add("activePoint");
//     return activePoint;
// };
//
// const getNextIndex = (activeSlide, slidersCount) => {
//     let nextIndex = parseInt(activeSlide.dataset.index) + 1;
//     let slidesContainer = activeSlide.parentElement;
//     let nextSlide = activeSlide.nextSibling;
//     if(!nextSlide) {
//         getAndMoveFirstSlideAtTheEnd(slidesContainer);
//     }
//     if(nextIndex > slidersCount) {
//         nextIndex = 1;
//     }
//     return nextIndex;
// };
//
// const getPrevIndex = (activeSlide, slidersCount) => {
//     let prevIndex = parseInt(activeSlide.dataset.index) - 1;
//     let slidesContainer = activeSlide.parentElement;
//     let previousSlide = activeSlide.previousSibling;
//     if(!previousSlide) {
//         getAndMoveLastSlideAtTheStart(slidesContainer);
//     }
//     if(prevIndex < 1) {
//         prevIndex = slidersCount;
//     }
//     return prevIndex
// };
//
// const addAnimationClassesForNextClick = (activeSlide, slidesToShow) => {
//     slidesToShow = 3;
//     let previousSlide = activeSlide.previousSibling;
//     let nextSlide = activeSlide.nextSibling;
//     let nextOfNextSlide = nextSlide.nextSibling;
//     if(slidesToShow > 2) {
//         try {
//             previousSlide.classList.add("prevOfPrev");
//         } catch(e) {
//
//         }
//         try {
//             nextOfNextSlide.classList.add("nextOfNext");
//         } catch(e) {
//
//         }
//     } else {
//         try {
//             activeSlide.classList.add("prevOfPrev");
//         } catch(e) {
//
//         }
//         try {
//             nextSlide.classList.add("nextOfNext");
//         } catch(e) {
//
//         }
//     }
// };
//
// const addAnimationClassesForPrevClick = (activeSlide, slidesToShow) => {
//     slidesToShow = 3;
//     let nextSlide = activeSlide.nextSibling;
//     let previousSlide = activeSlide.previousSibling;
//     let prevOfPrevSlide = previousSlide.previousSibling;
//     if(slidesToShow > 2) {
//        nextSlide.classList.add("nextOfNext1");
//         prevOfPrevSlide.classList.add("prevOfPrev1");
//     } else {
//         //activeSlide.classList.add("nextOfNext1");
//        previousSlide.classList.add("prevOfPrev1");
//     }
// };
//
// const getAndMoveFirstSlideAtTheEnd = (slidesContainer) => {
//     let firstSlide = slidesContainer.firstChild;
//     firstSlide.remove();
//     slidesContainer.appendChild(firstSlide);
//     return firstSlide;
// };
//
// const getAndMoveLastSlideAtTheStart = (slidesContainer) => {
//     let lastSlide = slidesContainer.lastChild;
//     lastSlide.remove();
//     slidesContainer.insertBefore(lastSlide, slidesContainer.firstChild);
//     return lastSlide;
// };
//
// export {
//     changeAndGetNextSlide,
//     changeAndGetPrevSlide,
//     removeActiveSliders,
//     addAndGetActiveSlider,
//     addNextAndPreviousSliders,
//     addNextSlider,
//     addPreviousSlider,
//     changeActivePoint,
//     moveSliders
// };
