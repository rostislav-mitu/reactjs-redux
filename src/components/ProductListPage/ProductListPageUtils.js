const checkBrand = (item, brand) => (
  item.name.includes(brand)
);

const checkPrice = (item, price) => (
  item.price >= price.from && item.price < price.to
);

const filterItems = (allItems, filter, checkByFilter) => {
  const checkBoxesValues = filter.value;
  let filteredItemsByBrand = allItems;
  if (checkBoxesValues.length !== 0) {
    filteredItemsByBrand = allItems.filter(item => {
      let isFiltered = false;
      checkBoxesValues.forEach(value => {
        isFiltered = checkByFilter(item, value) || isFiltered;
      });
      return isFiltered;
    });
  }
  return filteredItemsByBrand;
};

function getCommonItems(firstArray, secondArray) {
  const finalArray = firstArray.filter(item => secondArray.indexOf(item) > -1);
  if (finalArray.length === 0) {
    return [null];
  }
  return finalArray;
}

const getVisibleItems = () => {
  let filteredItemsByBrand = null;
  let filteredItemsByPrice = null;
  return (store, filter) => {
    switch (filter.key) {
      case 'SHOW_ALL':
        return store.items;
      case 'SHOW_BY_BRAND':
        filteredItemsByBrand = filterItems(store.items.slice(), filter, checkBrand);
        return getCommonItems(filteredItemsByBrand, filteredItemsByPrice || store.items);
      case 'SHOW_BY_PRICE':
        filteredItemsByPrice = filterItems(store.items.slice(), filter, checkPrice);
        return getCommonItems(filteredItemsByBrand || store.items, filteredItemsByPrice);
      default:
        return store.items;
    }
  };
};

export default getVisibleItems;
