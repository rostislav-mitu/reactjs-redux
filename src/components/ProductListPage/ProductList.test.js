/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import ProductList from './ProductList'
import NoResultByFilter from './NoResultByFilter'
import Product from './Product'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(items) {
    return shallow(<ProductList items={items}/>);
}

describe("ProductList", () => {
    it("should render 'NoResultByFilter' when items[0] is null", sinon.test(function() {
        const wrapper = setup([null]);
        expect(wrapper.find(NoResultByFilter).length).toBe(1);
        expect(wrapper.find(Product).length).toBe(0);
    }));
    it("should render 'Product' when items[0] is not null", sinon.test(function() {
        let items = [
            {
                id: 0
            },
            {
                id: 1
            },
            {
                id: 2
            }
        ];
        const wrapper = setup(items);
        expect(wrapper.find(NoResultByFilter).length).toBe(0);
        expect(wrapper.find(Product).length).toBe(items.length);
    }));
});