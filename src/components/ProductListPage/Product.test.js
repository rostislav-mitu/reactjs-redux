/* eslint-disable */
import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import sinonTest from 'sinon-test'
import Product from './Product'
import AddToCart from './AddToCart'
import ProductImage from './ProductImage'
import ProductName from './ProductName'
import ProductPrice from './ProductPrice'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup(item) {
    return shallow(<Product item={item}/>);
}

describe("ProductList", () => {
    let item = {
        id: 1
    };
    it("should render 'ProductImage'", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find(ProductImage).length).toBe(1);
    }));
    it("should render 'ProductName'", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find(ProductName).length).toBe(1);
    }));
    it("should render 'ProductPrice'", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find(ProductPrice).length).toBe(1);
    }));
    it("should render 'AddToCart'", sinon.test(function() {
        const wrapper = setup(item);
        expect(wrapper.find(AddToCart).length).toBe(1);
    }));
});