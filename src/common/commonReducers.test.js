import expect from 'expect'
import React from 'react'
import sinon from 'sinon'
import * as reducers from './commonReducers'
import * as actions from './commonActions'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

describe("commonReducers", () => {
    const initialState = {
        items: [
            {
                id: 1,
                price: 420,
                name: "name1",
                count: 1
            },
            {
                id: 2,
                price: 120,
                name: "name2",
                count: 2
            }
        ]
    };
    const verifyNewAndExpectedState = (newState, expectedResult) => {
        newState.items.forEach((item, index) => {
            expect(item.id).toBe(expectedResult.items[index].id);
            expect(item.price).toBe(expectedResult.items[index].price);
            expect(item.count).toBe(expectedResult.items[index].count);
        });
    };
    it("should add item to Cart correctly when item is not in cart", sinon.test(function() {
        const COUNT = 3;
        let item = {
            id: 3,
            name: "someName",
            price: 100
        };
        let expectedResult = {};
        expectedResult.items = initialState.items.slice();
        expectedResult.items.push(Object.assign({}, item, {
            count: COUNT
        }));
        const action = actions.addItemToCart(item, COUNT);
        const newState = reducers.cartReducer(initialState, action);
        verifyNewAndExpectedState(newState, expectedResult);
        expect(newState.items.length).toNotBe(initialState.items.length);
    }));

    it("should increment count when added item is already in the cart", sinon.test(function() {
        const COUNT = 2;
        const INDEX_OF_ITEM_TO_ADD = 1;
        let expectedResult = {};
        expectedResult.items = initialState.items.slice();
        expectedResult.items[INDEX_OF_ITEM_TO_ADD] = Object.assign({}, expectedResult.items[1]);
        expectedResult.items[INDEX_OF_ITEM_TO_ADD].count += COUNT;
        const action = actions.addItemToCart(initialState.items[1], COUNT);
        const newState = reducers.cartReducer(initialState, action);
        verifyNewAndExpectedState(newState, expectedResult);
        expect(newState.items[INDEX_OF_ITEM_TO_ADD].count).toNotBe(initialState.items[INDEX_OF_ITEM_TO_ADD].count);
    }));

    it("should delete item correctly from cart", sinon.test(function() {
        const INDEX_OF_ITEM_TO_DELETE = 1;
        const INDEX_OF_FIRST_ITEM = 0;
        let expectedResult = {};
        expectedResult.items = [];
        expectedResult.items.push(Object.assign({}, initialState.items[INDEX_OF_FIRST_ITEM]));
        const action = actions.deleteItemFromCard(initialState.items[INDEX_OF_ITEM_TO_DELETE]);
        const newState = reducers.cartReducer(initialState, action);
        verifyNewAndExpectedState(newState, expectedResult);
        expect(newState.items.length).toNotBe(initialState.items.length);
    }));

    it("should return the same state when action.type not corespond", sinon.test(function() {
        const action = {
            type: "DEFAULT",
            item: {},
            count: 0
        };
        const newState = reducers.cartReducer(initialState, action);
        expect(newState).toBe(initialState);
    }));
    it("should return the default state when action.type not corespond and state is undefined", sinon.test(function() {
        const action = {
            type: "DEFAULT",
            item: {},
            count: 0
        };
        const defaultState = {
            items: []
        };
        const newState = reducers.cartReducer(undefined, action);
        expect(newState.items.length).toBe(defaultState.items.length);
    }));
});