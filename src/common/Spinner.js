import React from 'react';

const Spinner = () => (
  <div>
    <h1>LOADING...</h1>
  </div>
);

export default Spinner;
