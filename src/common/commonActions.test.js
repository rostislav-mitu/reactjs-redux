import expect from 'expect'
import React from 'react'
import sinon from 'sinon'
import * as actions from './commonActions'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

describe("common -> Actions", () => {
    it("Should return the right action for addItemToCart", sinon.test(function() {
        let item = {
            id: "someID",
            price: 420
        };
        let count = 2;
        let expectedResult = {
            type: "ADD_ITEM",
            item,
            count
        };
        let action = actions.addItemToCart(item, count);
        expect(action).toEqual(expectedResult);
    }));
});