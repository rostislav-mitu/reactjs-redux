function addItemToCart(item, count) {
  return {
    type: 'ADD_ITEM',
    item,
    count
  };
}

function deleteItemFromCard(item) {
  return {
    type: 'DELETE_ITEM',
    item
  };
}

export {
  addItemToCart,
  deleteItemFromCard
};
