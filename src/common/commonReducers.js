const isItemInCart = (items, item) => {
  let isInCart = false;
  items.forEach((itemFromList) => {
    if (itemFromList.name === item.name) {
      isInCart = true;
    }
  });
  return isInCart;
};

const incrementItemCounter = (items, item, count) => {
  const itemIndex = items.indexOf(item);
  const itemsToReturn = items.slice();
  itemsToReturn[itemIndex] = Object.assign({}, item);
  itemsToReturn[itemIndex].count += count;
  return itemsToReturn;
};

const getExistentItemFromCart = (items, item) => {
  let existentItem;
  items.forEach((itemFromList) => {
    if (itemFromList.name === item.name) {
      existentItem = itemFromList;
    }
  });
  return existentItem;
};

const pushItemInCart = (items, item, count) => {
  const itemsToReturn = items.slice();
  itemsToReturn.push(Object.assign({}, item));
  itemsToReturn[items.length].count = count;
  return itemsToReturn;
};

const addItemToCart = (items, item, count) => {
  let itemsToReturn;
  if (isItemInCart(items, item, count)) {
    itemsToReturn = incrementItemCounter(items, getExistentItemFromCart(items, item), count);
  } else {
    itemsToReturn = pushItemInCart(items, item, count);
  }
  return itemsToReturn;
};

const deleteItemFromCart = (items, item) => {
  const index = items.indexOf(item);
  items.splice(index, 1);
};


function cartReducer(state = {
  items: []
}, action) {
  let items;
  switch (action.type) {
    case 'ADD_ITEM':
      items = state.items.slice();
      items = addItemToCart(items, action.item, action.count);
      return Object.assign({}, state, {
        items
      });
    case 'DELETE_ITEM':
      items = state.items.slice();
      deleteItemFromCart(items, action.item);
      return Object.assign({}, state, {
        items
      });
    default:
      return state;
  }
}

export {
    cartReducer
};
