import React from 'react';
import MediaQuery from 'react-responsive';
import PropTypes from 'prop-types';

const breakpoints = {
  smalldesktop: '(min-width: 1200px) and (max-width: 1799px)',
  desktop: '(min-width: 1200px)',
  bigDesktop: '(min-width: 1800px)',
  tablet: '(min-width: 600px) and (max-width: 1199px)',
  tabletPortrait: '(min-width: 600px) and (max-width: 899px)',
  tabletLandscape: '(min-width: 900px) and (max-width: 1199px)',
  phone: '(max-width: 599px)',
};
const Breakpoint = (props) => {
  const {
    name,
    children
  } = props;
  const breakpoint = breakpoints[name] || breakpoints.desktop;
  return (
    <MediaQuery {...props} query={breakpoint}>
      {children}
    </MediaQuery>
  );
};

Breakpoint.propTypes = {
  name: PropTypes.string,
  children: PropTypes.object
};

export default Breakpoint;
