import React from 'react'
import Breakpoint from './Breakpoint'
export default function TabletLandscapeBreakpoint(props) {
    return (
        <Breakpoint name="tabletPortrait">
            {props.children}
        </Breakpoint>
    );
};
