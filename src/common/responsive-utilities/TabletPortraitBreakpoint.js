import React from 'react'
import Breakpoint from './Breakpoint'
export default function TabletPortraitBreakpoint(props) {
    return (
        <Breakpoint name="tabletPortrait">
            {props.children}
        </Breakpoint>
    );
};
