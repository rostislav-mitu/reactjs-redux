import React from 'react';
import PropTypes from 'prop-types';
import Breakpoint from './Breakpoint';

export default function TabletBreakpoint(props) {
  return (
    <Breakpoint name="tablet">
      {props.children}
    </Breakpoint>
  );
}

TabletBreakpoint.propTypes = {
  children: PropTypes.object
};
