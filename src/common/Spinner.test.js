import expect from 'expect'
import React from 'react'
import { mount, shallow } from 'enzyme'
import sinon from 'sinon'
import Spinner from './Spinner'
import sinonTest from 'sinon-test'

sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

function setup() {
    return shallow(<Spinner/>);
}

describe("common -> <Spinner/>", () => {
    it("Should render correctly itself", sinon.test(function() {
        const wrapper = setup();
        expect(wrapper.exists()).toBe(true);
    }));
});