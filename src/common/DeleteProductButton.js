import React from 'react';
import PropTypes from 'prop-types';
import { deleteItemFromCard } from './commonActions';

class DeleteProductButton extends React.Component {
  deleteItem(item) {
    const { store } = this.context;
    store.dispatch(deleteItemFromCard(item));
  }
  render() {
    const { item } = this.props;
    return (
      <div>
        <button onClick={() => this.deleteItem(item)}>
            Delete
        </button>
      </div>
    );
  }
}

DeleteProductButton.contextTypes = {
  store: PropTypes.object
};

DeleteProductButton.propTypes = {
  item: PropTypes.object
};

export default DeleteProductButton;
